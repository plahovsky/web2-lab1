const express = require('express');
const app = express();
const port = process.env.PORT || 3000;
const path = require('path');
require('dotenv').config();
const { auth, requiresAuth } = require('express-openid-connect');
app.use(express.json({ limit: '1mb' }));

var user1 = {
    'name' : 'Testni korisnik broj 1',
    'email' : 'emailkorisnika1@gmail.com',
    'lat': 44,
    'lon' : 15,
    'time' : '22:00:12'
};

var user2 = {
    'name': 'Testni korisnik broj 2',
    'email': 'emailkorisnika2@fer.hr',
    'lat': 45,
    'lon': 15,
    'time': '22:00:12'
};

var user3 = {
    'name': 'Tesni korisnik broj 3',
    'email': 'emailtrecegkorisnika@hotmail.com',
    'lat': 44,
    'lon': 16,
    'time': '22:00:12'
};

var user4 = {
    'name': 'Testni korisnik broj 4',
    'email': 'emailkorisnika4@gmail.com',
    'lat': 45,
    'lon': 16,
    'time': '22:00:12'
};

var user5 = {
    'name': 'Testni korisnik broj 5',
    'email': 'emailkorisnikabroj5@fer.hr',
    'lat': 45,
    'lon': 17,
    'time': '22:00:12'
};

var korisnici = { user1, user2, user3, user4, user5 };

app.use(
    auth({
        authRequired: true,
        auth0Logout: true,
        issuerBaseURL: process.env.ISSUER_BASE_URL,
        baseURL: process.env.BASE_URL,
        clientID: process.env.CLIENT_ID,
        secret: process.env.SECRET,
        /*idpLogout: true,*/
    })
);

app.get('', requiresAuth(), (req, res) => {
    if (req.oidc.isAuthenticated()) {
        res.sendFile(path.join(__dirname + '/index.html'));
    }
    else {
        res.send('Nisi ulogiran');
    }
});

app.post('/geo', (req, res) => {
    if (korisnici.user5.email != req.oidc.user.email) {
        korisnici.user1 = korisnici.user2;
        korisnici.user2 = korisnici.user3;
        korisnici.user3 = korisnici.user4;
        korisnici.user4 = korisnici.user5;
        korisnici.user5 = {
            'name': req.oidc.user.name,
            'email': req.oidc.user.email,
            'lat': req.body.latitude,
            'lon': req.body.longitude,
            'time': Date(req.body.ts)
        };
        res.json(korisnici);
        //console.log(korisnici);
    }



});

app.listen(port, () => {
    console.log('listening on port: ' + port);
});